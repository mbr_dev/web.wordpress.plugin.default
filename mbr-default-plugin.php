<?php
/*
Plugin Name: MBR Default Plugin
Plugin URI:  http://URI_Of_Page_Describing_Plugin_and_Updates
Description: This is a basic Plugin
Version:     1.5
Author:      Manuel Rösch
Author URI:  http://URI_Of_The_Plugin_Author
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Domain Path: /languages
Text Domain: mbr-default-plugin
*/

function mbrdeftagfunct(  ) {
//    Output goes here
    return "This is a default Plugin Content";
}


add_shortcode( 'mbr-def-tag', 'mbrdeftagfunct' );